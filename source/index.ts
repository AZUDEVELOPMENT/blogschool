import Config from 'config';
import Fs from 'fs';
import Path from 'path';
import 'reflect-metadata';
import { Connection, createConnection } from 'typeorm';
import { LocalAuthenticator } from './authentication';
import { IServerConfig } from './interfaces';
import Server from './server';

try {
  (async () => {
    // SSL Private key
    const key: Buffer = Fs.readFileSync(Path
      .resolve(__dirname, '..', 'localhost-privkey.pem'));

    // SSL Certification
    const cert: Buffer = Fs.readFileSync(Path
      .resolve(__dirname, '..', 'localhost-cert.pem'));

    // TypeORM database connection
    const connection: Connection = await createConnection({
      ...(Config.get<any>('orm')),
      name: 'serverConnection',
      entities: [
        Path.resolve(__dirname, './entities/*.js'),
        Path.resolve(__dirname, './entities/*.ts')
      ]
    });

    // Local Passport Authenticator
    const localAuthenticator: LocalAuthenticator =
      new LocalAuthenticator(connection);

    localAuthenticator.setupAuthenticator({
      ...(Config.get<any>('localAuth'))
    }, () => {
      // Express server
      const server: Server = new Server(Config
        .get<IServerConfig>('webserver'));
      server.startServer(key, cert, (error, port) => {
        if (error)
          console.log(error);
        console.log(`Listening on port: ${port}`);
      });
    });
  })();
} catch (error) {
  console.error(error);
}