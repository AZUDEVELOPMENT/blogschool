import Express from 'express';
import Session from 'express-session';
import Passport from 'passport';
import Spdy from 'spdy';
import { IServerConfig } from './interfaces';
import Router from './router';

export default class Server {
  private readonly app: Express.Application = Express();
  private sessionStore: Session.MemoryStore =
    new Session.MemoryStore();
  constructor(
    private serverConfig: IServerConfig
  ) {
    this.setup();
  }

  private setup() {
    this.serverConfig.viewEngine &&
      this.app.set('view engine', this.serverConfig.viewEngine);
    this.serverConfig.viewPath &&
      this.app.set('views', this.serverConfig.viewPath);
    this.app.use('/static', Express
      .static(this.serverConfig.staticPath));
    this.app.use(require('cookie-parser')(
      this.serverConfig.secret
    ));
    this.app.use(require('body-parser').json());
    this.app.use(require('body-parser')
      .urlencoded({ extended: false }))
    this.app.use(Session({
      secret: this.serverConfig.secret,
      store: this.sessionStore,
      resave: false,
      saveUninitialized: false
    }));
    this.app.use(require('req-flash')());
    this.app.use(Passport.initialize());
    this.app.use(Passport.session());
    this.app.use(Router);
  }

  public startServer(
    key: Buffer,
    cert: Buffer,
    callback?: (error: Error, port?: number) => void
  ) {
    Spdy.createServer({
      key, cert
    }, this.app).listen(this.serverConfig.port, (error: Error) => {
      if (error)
        callback(error)
      callback(null, this.serverConfig.port);
    });
  }
}