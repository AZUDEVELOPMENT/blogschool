import CEL from 'connect-ensure-login';
import { Router } from 'express';
import Passport from 'passport';
import * as Controllers from './controllers';
import * as Middlewares from './middlewares';

const router: Router = Router();
const loggedOutRoute = '/';
const loggedInRoute = '/posts';

// MIDDLEWARES
router.use(Middlewares.IsAuthenticated);

// GET ROUTES
router.get('/', CEL.ensureLoggedOut({
  redirectTo: loggedInRoute
}), Controllers.IndexGet);

router.get('/posts', CEL.ensureLoggedIn({
  redirectTo: loggedOutRoute
}), Controllers.PostsGet);

router.get('/registration', CEL.ensureLoggedOut({
  redirectTo: loggedInRoute
}), Controllers.RegistrationGet);

// POST ROUTES
router.post('/login', Passport.authenticate('local', {
  successRedirect: loggedInRoute,
  failureRedirect: loggedOutRoute,
  failureFlash: true
}));

router.post('/registration', CEL.ensureLoggedOut({
  redirectTo: loggedInRoute
}), Controllers.RegistrationPost);

router.post('/posts', CEL.ensureLoggedIn({
  redirectTo: loggedOutRoute
}), Controllers.PostsPost);

// DELETE ROUTES
router.post('/logout', CEL.ensureLoggedIn({
  redirectTo: loggedOutRoute
}), Controllers.LogoutDelete);

export default router;