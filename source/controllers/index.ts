export * from './index.get';
export * from './logout.delete';
export * from './posts.get';
export * from './posts.post';
export * from './registration.get';
export * from './registration.post';