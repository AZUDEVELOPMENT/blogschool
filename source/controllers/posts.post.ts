import { NextFunction, Request, Response } from 'express';
import { Connection, Repository, getConnection } from 'typeorm';
import { User, Post } from '../entities';

export const PostsPost = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const dbConnection: Connection = getConnection('serverConnection');
  if (!dbConnection.isConnected)
    await dbConnection.connect();
  const postText: string = req.query.postText ||
    req.params.postText || req.body.postText;
  const userRepository: Repository<User> =
    dbConnection.getRepository(User);
  let author: User = await userRepository.findOne({ ...(req.user) });
  const postRepository: Repository<Post> =
    dbConnection.getRepository(Post);
  let post: Post = new Post;
  post.author = author;
  post.postText = postText;
  post.date = new Date();
  postRepository.save(post).then(async post => {
    let posts: Post[] = await postRepository.find({
      relations: ['author']
    });
    res.render('posts', { message: req.flash('error'), posts });
  }).catch(async reason => {
    let posts: Post[] = await postRepository.find({
      relations: ['author']
    });
    res.render('posts', { message: reason, posts });
  });
};