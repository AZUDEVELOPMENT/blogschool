import { NextFunction, Request, Response } from 'express';

export const LogoutDelete = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  req.logout();
  res.redirect('/');
};