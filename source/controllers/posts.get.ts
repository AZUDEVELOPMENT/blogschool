import { NextFunction, Request, Response } from 'express';
import { Connection, Repository, getConnection } from 'typeorm';
import { Post } from '../entities';

export const PostsGet = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const dbConnection: Connection = getConnection('serverConnection');
  if (!dbConnection.isConnected)
    await dbConnection.connect();
  const postRepository: Repository<Post> =
    dbConnection.getRepository(Post);
  let posts: Post[] = await postRepository.find({
    relations: ['author']
  });
  res.render('posts', { message: req.flash('error'), posts });
};