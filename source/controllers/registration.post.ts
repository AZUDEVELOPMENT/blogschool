import { NextFunction, Request, Response } from 'express';
import { Connection, Repository, getConnection } from 'typeorm';
import BCrypt from 'bcrypt';
import { User } from '../entities';

export const RegistrationPost = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const dbConnection: Connection = getConnection('serverConnection');
  if (!dbConnection.isConnected)
    await dbConnection.connect();
  const userRepository: Repository<User> =
    dbConnection.getRepository(User);
  const username: string = req.query.regUsername ||
    req.params.regUsername || req.body.regUsername;
  let user: User = await userRepository.findOne({ username });
  if (user) res.render('registration', {
    message: 'This username is already be claimed!'
  });
  else {
    const password: string = req.query.regPassword ||
      req.params.regPassword || req.body.regPassword;
    const passwordHash: string = BCrypt.hashSync(password, 10);
    const displayName: string = req.query.regDisplayName ||
      req.params.regDisplayName || req.body.regDisplayName;
    user = new User();
    user.username = username;
    user.password = passwordHash;
    user.displayName = displayName;
    userRepository.save(user).then(user => req.login(user, (err) => {
      if (!err)
        res.redirect('posts');
      else
        res.render('registration', { message: err });
    })).catch(reason => res.render('registration', { message: reason }));
  }
};