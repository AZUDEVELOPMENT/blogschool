import { NextFunction, Request, Response } from 'express';

export const IndexGet = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.render('index', { message: req.flash('error') });
};