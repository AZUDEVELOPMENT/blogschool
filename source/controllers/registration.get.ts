import { NextFunction, Request, Response } from 'express';

export const RegistrationGet = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.render('registration', { message: req.flash('error') });
};