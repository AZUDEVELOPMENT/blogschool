import Passport from 'passport';
import { Connection } from 'typeorm';
import { User } from '../entities';
import { IBaseAuthenticator } from '../interfaces';

export abstract class BaseAuthenticator implements IBaseAuthenticator {
  constructor(
    protected dbConnection: Connection
  ) {
    if (!dbConnection.isConnected)
      throw new Error('Database is disconnected!');
    Passport.serializeUser(this.serializeUser.bind(this));
    Passport.deserializeUser(this.deserializeUser.bind(this));
  }

  public serializeUser(
    user: User,
    callback: (error: Error, userid?: number) => void
  ) {
    callback(null, user.id);
  };

  public deserializeUser(
    userid: number,
    callback: (error: Error, user?: User) => void
  ) {
    let userRepository = this.dbConnection.getRepository(User);
    userRepository.findOne(userid)
      .then(user => callback(null, user))
      .catch(reason => callback(reason));
  };
}