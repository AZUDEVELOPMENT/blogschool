import Passport from 'passport';
import LocalAuth from 'passport-local';
import { User } from '../entities';
import { ILocalAuthenticator } from '../interfaces';
import { BaseAuthenticator } from './authenticator';
import BCrypt from 'bcrypt';

export class LocalAuthenticator extends BaseAuthenticator implements ILocalAuthenticator {
  public setupAuthenticator(
    config: LocalAuth.IStrategyOptions | null,
    callback: () => void
  ) {
    Passport.use(new LocalAuth.Strategy(config, this.authenticator.bind(this)));
    callback();
  }

  public authenticator(
    username: string,
    password: string,
    callback?: (
      error: Error,
      user: User,
      option?: LocalAuth.IVerifyOptions
    ) => void
  ) {
    const userRepository = this.dbConnection.getRepository(User);
    userRepository.findOne({ username })
      .then(user => {
        if (user && BCrypt.compareSync(password, user.password))
          callback(null, user);
        else
          callback(null, null, { message: 'Wrong username or password' });
      }).catch(reason => callback(reason, null));
  };
}