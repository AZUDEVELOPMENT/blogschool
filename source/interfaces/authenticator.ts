import { User } from '../entities';

export type TSerializeUserCallback = (
  error: Error,
  userid?: number
) => void

export type TDeserializeUserCallback = (
  error: Error,
  user?: User
) => void

export interface IBaseAuthenticator {
  serializeUser: (
    user: User,
    callback: TSerializeUserCallback
  ) => void;

  deserializeUser: (
    userid: number,
    callback: TDeserializeUserCallback
  ) => void;
}

