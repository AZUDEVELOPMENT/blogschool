import LocalAuth from 'passport-local';
import { User } from '../entities';

export interface IServerConfig {
  port: number;
  secret: string;
  viewPath: string;
  viewEngine: string;
  staticPath: string;
}