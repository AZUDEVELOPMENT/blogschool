import { NextFunction, Request, Response } from 'express';

export const IsAuthenticated = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.locals.isAuthenticated = req.isAuthenticated();
  if (res.locals.isAuthenticated)
    res.locals.displayname = req.user.displayName; // TODO: Should be chnageg to displayname
  next();
};