# Surf

Surf with TypeORM, Passport and HTTP/2 Support written in Typescript.

## Configure the project

### Fields

* **ORM** : This one is the all ORM configuartion, you find more info right [here](https://github.com/typeorm/typeorm/blob/master/docs/using-ormconfig.md)

* **local** : This one is our authentication system configuration, you find more info right [here](https://github.com/jaredhanson/passport-local#parameters)

* **webserver** : This one is just contain our webserver's port

* **test** : This one is for the tests, so this is optional, for production and developemtn build, onlt required for test build.

## Before you build the project, you need SSL key and SSL Cert

### If you not have SSL Key and Cert and you not have a public domain for this system

```sh
./genkey.sh
```

### If you not have SSL Key and Cert but you have a public domain for this system

[Generate a key with Let's Encrypt](https://letsencrypt.org/getting-started)

### If you have SSL Key and Cert

```sh
cp {originalKeyPath} ${PWD}/localhost-privkey.pem
cp {originalCertPath} ${PWD}/localhost-cert.pem
```

## Build project [DEVELOPMENT]

### 1. Step: Compile Typescript to Javascript in debug mode

```sh
npm run build
```

### 2. Step: Open a new terminal or a new command prompt and run the debug/index.js

```sh
npm run debug
```

## Build project [TEST]

### 1. Step: Host a selenium server for the test

### If you use docker, but you should because this is the recommend way

```sh
sudo docker run -d -p 4444:4444 --net=host -h 127.0.0.1 --shm-size=2g selenium/standalone-chrome:3.12.0-cobalt
```

### If you not use docker, you can find the jar file [here](http://selenium-release.storage.googleapis.com/index.html), and also you have to install the chrome driver

```sh
java -jar selenium-server-standalone-x.x.x.jar
```

### 2. Step: Open a new terminal or a new command prompt and run tests

```sh
npm test
```

## Build project [PRODUCTION]

### 1. Step: Build docker image

```sh
docker build -t surf_image .
```

### 2. Step: Run docker image with exposed port

```sh
docker run -d -p 443:8000 --name surf_container surf_image
```

## Frequently Asked Questions

### [How to change User authentication](https://bitbucket.org/azuweyapp/nodejs-boilerplate/wiki/How-to-change-user-authentication)